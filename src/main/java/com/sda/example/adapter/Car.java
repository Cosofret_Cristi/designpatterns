package com.sda.example.adapter;

public interface Car {
    double calculateSpeed();
}
