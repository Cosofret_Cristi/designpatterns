package com.sda.example.adapter;

public class CarAdapterImpl implements CarAdapter {
    private Car luxuryCars;

    public CarAdapterImpl(Car luxuryCars) {
        this.luxuryCars = luxuryCars;
    }

    @Override
    public double calculateSpeed() {
        return convertMPHtoKMPH(luxuryCars.calculateSpeed());
    }

    private double convertMPHtoKMPH(double mph) {
        return mph * 1.60934;
    }
}