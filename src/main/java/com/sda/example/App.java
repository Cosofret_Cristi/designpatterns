package com.sda.example;


import com.sda.example.adapter.BugattiVeyron;
import com.sda.example.adapter.CarAdapter;
import com.sda.example.adapter.CarAdapterImpl;
import com.sda.example.observer.GreenObserver;
import com.sda.example.observer.RedObserver;
import com.sda.example.observer.Subject;
import com.sda.example.observer.YellowObserver;
import com.sda.example.proxy.Car;
import com.sda.example.proxy.ProxyAudi;

/**
 * Hello world!
 *
 */
public class App {

    public static void main( String[] args ) {

        // factory

        /*CarFactory factory = new CarFactory();

        Car skoda = factory.produceCar("Skoda");
        skoda.makeNoise();

        Car skoda2 = factory.produceCar("Skoda");

        System.out.println(skoda);
        System.out.println(skoda2);

        Car audi = factory.produceCar("Audi");
        audi.makeNoise();

        Car smart = factory.produceCar("Smart");
        smart.makeNoise();*/


        // builder

        /*Car car = new Car.Builder().cuCuloarea("roz").cuModelul("Audi").build();

        System.out.println(car);*/


        // adapter

        /*Car bugattiVeyron = new BugattiVeyron();

        CarAdapter bugattiVeyronAdapter = new CarAdapterImpl(bugattiVeyron);

        System.out.println(bugattiVeyronAdapter.calculateSpeed());*/


        // proxy

        Car car = new ProxyAudi("red");

        car.display();

        car.display();

        // Observer

        /*Subject subject = new Subject();

        YellowObserver yellowObs = new YellowObserver(subject);
        new GreenObserver(subject);
        new RedObserver(subject);

        System.out.println("First state change");
        subject.setValue("Ala Bala Portocala!");

        System.out.println();
        System.out.println("Second state change");
        subject.setValue("Ana are mere!");
*/
    }

}
