package com.sda.example.singleton;

public class SingletonExample {

    private int number;

    private static SingletonExample instance;

    private SingletonExample() {}

    public static SingletonExample getInstance() {
        if(instance == null) {
            instance = new SingletonExample();
        }
        return instance;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
