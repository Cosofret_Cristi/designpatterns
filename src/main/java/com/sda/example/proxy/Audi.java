package com.sda.example.proxy;

public class Audi implements Car {

    public String color;

    public Audi(String color) {
        this.color = color;
        createCar();
    }

    @Override
    public void display() {
        System.out.println("Displaying car with color " + color);
    }

    private void createCar() {
        System.out.println("Audi car with color " + color + " was created!");
    }
}
