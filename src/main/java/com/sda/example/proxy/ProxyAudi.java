package com.sda.example.proxy;

public class ProxyAudi implements Car {

    private Audi audi;
    private String color;

    public ProxyAudi(String color){
        this.color = color;
    }

    @Override
    public void display() {
        if(audi == null){
            audi = new Audi(color);
        }
        audi.display();
    }

}
