package com.sda.example.factory;

public interface Car {

    void run();

    void makeNoise();

}
