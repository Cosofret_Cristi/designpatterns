package com.sda.example.factory;

public class Skoda implements Car {

    public void run() {
        System.out.println("Skoda run!");
    }

    public void makeNoise() {
        System.out.println("Some Skoda noise!");
    }
}
