package com.sda.example.factory;

public class Audi implements Car {

    @Override
    public void run() {
        System.out.println("Audi run!");
    }

    @Override
    public void makeNoise() {
        System.out.println("Audi noise!");
    }
}
