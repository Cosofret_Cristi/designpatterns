package com.sda.example.factory;

public class CarFactory {

    public Car produceCar(String type) {
        switch (type) {
            case "Audi":
                return new Audi();
            case "Skoda":
                return new Skoda();
            default:
                throw new IllegalArgumentException("Car do not exist!");
        }
    }

}
