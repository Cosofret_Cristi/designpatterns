package com.sda.example.observer;

public class YellowObserver extends Observer{

    public YellowObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Yellow Observer say: Saw that value became " + subject.getValue() );
    }
}
