package com.sda.example.observer;

public class GreenObserver extends Observer{

    public GreenObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Green Observer say: Saw that value became " + subject.getValue() );
    }
}