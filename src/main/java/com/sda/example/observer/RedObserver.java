package com.sda.example.observer;

public class RedObserver extends Observer {

    public RedObserver(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println( "Red Observer say: Saw that value became " + subject.getValue() );
    }

}
