package com.sda.example.builder;

public class Car {

    private int numarRoti;
    private String culoare;
    private String marca;
    private String model;

    @Override
    public String toString() {
        return "Car{" +
                "numarRoti=" + numarRoti +
                ", culoare='" + culoare + '\'' +
                ", marca='" + marca + '\'' +
                ", model='" + model + '\'' +
                '}';
    }

    public static class Builder {

        private int numarRoti;
        private String culoare;
        private String marca;
        private String model;

        public Builder cuRotile(int numarRoti) {
            this.numarRoti = numarRoti;

            return this;
        }

        public Builder cuCuloarea(String culoare) {
            this.culoare = culoare;

            return this;
        }

        public Builder cuMarca(String marca) {
            this.marca = marca;

            return this;
        }

        public Builder cuModelul(String model) {
            this.model = model;

            return this;
        }

        public Car build() {
            Car car = new Car();

            car.numarRoti = this.numarRoti;
            car.culoare = this.culoare;
            car.marca = this.marca;
            car.model = this.model;

            return car;
        }
    }

}
